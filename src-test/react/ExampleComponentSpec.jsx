'use strict';

import React from 'react';
import TestUtils from 'react/lib/ReactTestUtils';
import ExampleComponent from '../../src/react/ExampleComponent'

describe('ExampleComponent', function () {

    var instance,
        container;

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
    });

    afterEach(() => {
        document.body.removeChild(container);
    });

    describe("weird", () => {
        it('should write horse', function () {
            instance = React.render(<ExampleComponent>Horse</ExampleComponent>, container);
            expect(React.findDOMNode(instance).innerText).toBe("Horse");
        });
    });
});