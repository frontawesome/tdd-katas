import FizzBuzz from '../../src/katas/FizzBuzz';

describe('FizzBuzz', () => {

    describe('run', () => {

        it('returns an array where all multiples of 3 are replaced with fizz', () => {
            expect(FizzBuzz.run(3)).toEqual([1, 2, 'fizz']);
        });

        it('returns an array where all multiples of 5 are replaced with buzz', () => {
            expect(FizzBuzz.run(5)).toEqual([1, 2, 'fizz', 4, 'buzz']);
        });

        it('returns an array where multiples of 5 and 3 are replaced with fizzbuzz', () => {
            expect(FizzBuzz.run(15)).toEqual(
                [1, 2, 'fizz', 4, 'buzz', 'fizz', 7, 8, 'fizz', 'buzz', 11, 'fizz', 13, 14, 'fizzbuzz']
            );
        });
    });
});
